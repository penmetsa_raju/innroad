﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee_management_system
{
    public class EmployeeInfo
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public int gender { get; set; }
        public string _gender { get; set; }
        public int age { get; set; }
        public double salary { get; set; }
        public int employee_type { get; set; }
        public string _employee_type { get; set; }
    }
}
