﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace Employee_management_system
{
    public delegate void del(double a, EmployeeInfo emp);
    class Employee : Employee_operation
    {
        static void Main(string[] args)
         {
            Employee employee = new Employee();
            List<EmployeeInfo> infos = new List<EmployeeInfo>();
            bool Exit = true;

            do
            {
                Console.Clear();
                Console.WriteLine("------Welcome------\nPlease select the option");
                Console.WriteLine("Press 1 to add an employee");
                Console.WriteLine("Press 2 to get details of all employees");
                Console.WriteLine("Press 3 to update an employee");
                Console.WriteLine("Press 4 to delete an employee by entering Employee email address");
                Console.WriteLine("Press 5 to delete employees by entering Employee Name");
                Console.WriteLine("Press 6 to get details of a specific employee ");
                Console.WriteLine("Press 7 to get Total Salary of an employee");
                Console.WriteLine("Press any other key to exit.");
                int op = Convert.ToInt32(Console.ReadLine());
                
                switch (op)
                {
                    case 1:
                        employee.Add_emp(infos);
                        break;
                    case 2:
                        employee.Display_emp(infos);
                        break;
                    case 3:
                        employee.Update_emp(infos);                        
                        break;
                    case 4:
                        employee.Del_mail_emp(infos);
                        break;
                    case 5:
                        employee.Del_name_emp(infos);
                        break;
                    case 6:
                        employee.Detail_emp(infos);
                            break;
                    case 7:
                        employee.salary_emp(infos);
                        break;
                    default:
                        Exit = false;
                        Console.WriteLine("Invali input");
                        break;
                }
            }while (Exit);
        }
    }
}
