﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee_management_system
{
    class Employee_operation
    {
        public void Add_emp(List<EmployeeInfo> infos)
        {
            EmployeeInfo input = new EmployeeInfo();
            Console.Clear();
            Console.WriteLine("Please enter First Name:");
            input.firstname = Console.ReadLine();
            Console.WriteLine("Please enter Last Name:");
            input.lastname = Console.ReadLine();
            Console.WriteLine("Please enter Email ID");
            input.email = Console.ReadLine();
            {
                while (Check_mail(infos, input.email))
                {
                    input.email = Console.ReadLine();
                }
            }
            Console.WriteLine("Please enter Gender \nEnter 0 for Male \nEnter 1 for Female \nEnter 2 for Other");
            input.gender = Convert.ToInt16(Console.ReadLine());
            {
                if (input.gender == 0)
                {
                    input._gender = "Male";
                }
                else if (input.gender == 1)
                {
                    input._gender = "Female";
                }
                else if (input.gender == 2)
                {
                    input._gender = "Other";
                }
            }
            Console.WriteLine("Please enter Age");
            input.age = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Please enter Salary");
            input.salary = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter Employee Type\nEnter 0 for Permanent Employee \nEnter 1 for Contractual Employee");
            input.employee_type = Convert.ToInt16(Console.ReadLine());
            {
                if (input.employee_type == 0)
                {
                    input._employee_type = "Permanent";
                }
                else if (input.employee_type == 1)
                {
                    input._employee_type = "Contractual";
                }
            }
            infos.Add(input);
            Console.WriteLine("Employee Details Added");
            Console.ReadKey(true);
        }

        public void Display_emp(List<EmployeeInfo> infos)
        {
            Console.Clear();
            Console.WriteLine("First Name   \tLast Name   \tEmail ID   \tGender   \tAge   \tSalary    \tEmployee Type");

            foreach (EmployeeInfo i in infos)
            {
                Console.WriteLine(i.firstname + "      \t" + i.lastname + "      \t" + i.email + "      \t" + i._gender + "      \t" + i.age + "    \t" + i.salary + "      \t" + i._employee_type + "Employee");
            }
            Console.ReadKey(false);
        }

        public void Update_emp(List<EmployeeInfo> infos)
        {
            string search;
            Console.WriteLine("Enter the employee email address");
            search = Console.ReadLine();
            foreach (var emp in infos)
            {
                if (emp.email == search)
                {
                    Console.WriteLine("Please enter First Name to update, existing first name is {0}", emp.firstname);
                    emp.firstname = Console.ReadLine();
                    Console.WriteLine("Please enter Last Name to update, existing last name is {0}", emp.lastname);
                    emp.lastname = Console.ReadLine();
                    Console.WriteLine("Please enter Gender to update, existing gender is {0} \nEnter 0 for Male \nEnter 1 for Female \nEnter 2 for Other", emp._gender);
                    emp.gender = Convert.ToInt16(Console.ReadLine());
                    {
                        if (emp.gender == 0)
                        {
                            emp._gender = "Male";
                        }
                        else if (emp.gender == 1)
                        {
                            emp._gender = "Female";
                        }
                        else if (emp.gender == 2)
                        {
                            emp._gender = "Other";
                        }
                    }
                    Console.WriteLine("Please enter Age");
                    emp.age = Convert.ToInt16(Console.ReadLine());
                    Console.WriteLine("Please enter Salary to update, existing salary is {0}", emp.salary);
                    emp.salary = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Please enter Employee Type to update, existing employee type is {0} \nEnter 0 for Permanent Employee \nEnter 1 for Contractual Employee", emp._employee_type);
                    emp.employee_type = Convert.ToInt16(Console.ReadLine());
                    {
                        if (emp.employee_type == 0)
                        {
                            emp._employee_type = "Permanent";
                        }
                        else if (emp.employee_type == 1)
                        {
                            emp._employee_type = "Contractual";
                        }
                    }
                    Console.WriteLine("Employee Details Updated");
                }
                else
                {
                    Console.WriteLine("Entered employee doesn't exist");
                    Console.ReadKey();
                }
            }
        }

        public void Del_mail_emp(List<EmployeeInfo> infos)
        {
            string search;
            Console.WriteLine("Enter the employee email address");
            search = Console.ReadLine();
            foreach (var emp in infos)
            {
                infos.Remove(emp);
                break;
            }
            Console.WriteLine("Employee Details Deleted");
        }

        public void Del_name_emp(List<EmployeeInfo> infos)
        {
            string search;
            Console.WriteLine("Enter the employee first name");
            search = Console.ReadLine();
            foreach (var emp in infos)
            {
                if (emp.firstname == search)
                {
                    infos.Remove(emp);
                    break;
                }
            }
            Console.WriteLine("Employee Details Deleted");
        }

        public void Display_emp(List<EmployeeInfo> infos, string email)
        {
            bool Flag = true;
            foreach (var emp in infos)
            {
                if (emp.email == email)
                {
                    Console.WriteLine(emp.firstname + "\t" + emp.lastname + "\t" + emp._gender + "\t" + emp.age + "\t" + emp.salary + "\t" + emp._employee_type);
                    Console.ReadLine();
                    Flag = false;
                    break;
                }
            }
            if (Flag == true)
            {
                Console.WriteLine("Employee not found!");
            }
        }
        public void Detail_emp(List<EmployeeInfo> infos)
        {
            string search;
            Console.WriteLine("Enter the employee email address");
            search = Console.ReadLine();
            Display_emp(infos, search);

        }

        public void salary_emp(List<EmployeeInfo> infos)
        {
            string search;
            Console.WriteLine("Please enter the employee email address");
            search = Console.ReadLine();
            foreach (var emp in infos)
            {
                if (emp.email == search)
                {
                    if (emp.employee_type == 0)
                    {
                        del d = new del(per_sal);
                        d(emp.salary, emp);
                    }
                    else if (emp.employee_type == 1)
                    {
                        del d = new del(con_sal);
                        d(emp.salary, emp);
                    }
                }
                Console.ReadKey();
            }
        }

        public void per_sal(double a, EmployeeInfo emp)
        {

            var sal = (emp.salary * 12) + (emp.salary * 0.08);
            Console.WriteLine(sal);
        }

        public void con_sal(double a, EmployeeInfo emp)
        {

            var sal = (emp.salary * 12);
            Console.WriteLine(sal);
        }

        public static bool Check_mail(List<EmployeeInfo> infos, string check)
        {
            foreach (var emp in infos)
            {
                if (emp.email == check)
                {
                    Console.WriteLine("email already is taken, Please choose another one");
                    return true;
                }
            }
            return false;
        }
    }
}
