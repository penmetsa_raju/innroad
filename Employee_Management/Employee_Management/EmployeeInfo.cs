﻿using System;
using System.Collections.Generic;

namespace Employee_Management
{
    class EmployeeInfo
    {
        public List<EmployeeInfo> infos = new List<EmployeeInfo>();
        public Dictionary<string, EmployeeInfo> KeyValues = new Dictionary<string, EmployeeInfo>();
        public string F_Name { get; set; }
        public string L_Name { get; set; }
        public string Email { get; set; }
        public double Salary { get; set; }
    }
}
