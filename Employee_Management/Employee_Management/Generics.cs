﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee_Management
{
    class Gen
    {
        public void check<T>(T Check)
        {
            EmployeeInfo info = new EmployeeInfo();
            Employee_operation ope = new Employee_operation();
            if (Check.GetType().ToString().Equals("System.Collections.Generic.List`1[Employee_Management.EmployeeInfo]"))
            {
                bool Exit = true;

                do
                {
                    Console.WriteLine("Press 1 to Add an Employee");
                    Console.WriteLine("Press 2 to Delete an Employee");
                    Console.WriteLine("Press 3 to Get details of an Employee");
                    Console.WriteLine("Press 4 to Get details of all Employee");
                    Console.WriteLine("Press any other character to exit from the Collections");
                    int opt = Convert.ToInt16(Console.ReadLine());


                    switch (opt)
                    {
                        case 1:
                            ope.Add_emp_Data(info.infos);
                            break;
                        case 2:
                            ope.Delete_emp(info.infos);
                            break;
                        case 3:
                            ope.Details_emp(info.infos);
                            break;
                        case 4:
                            ope.Details_all_emp(info.infos);
                            break;
                        default:
                            Exit = false;
                            break;
                    }
                } while (Exit);
            }

            if (Check.GetType().ToString().Equals("System.Collections.Generic.Dictionary`2[System.String,Employee_Management.EmployeeInfo]"))
            {
                bool Exit = true;

                do
                {
                    Console.WriteLine("Press 1 to Add an Employee");
                    Console.WriteLine("Press 2 to Delete an Employee");
                    Console.WriteLine("Press 3 to Get details of an Employee");
                    Console.WriteLine("Press 4 to Get details of all Employee");
                    Console.WriteLine("Press any other character to exit from the Collections");
                    int opt = Convert.ToInt16(Console.ReadLine());

                    switch (opt)
                    {
                        case 1:
                            ope.Add_emp_dic();
                            break;
                        case 2:
                            ope.Del_emp_dic();
                            break;
                        case 3:
                            ope.Detail_emp_dis();
                            break;
                        case 4:
                            ope.Dis_emp_dic();
                            break;
                        default:
                            break;
                    }
                } while (Exit);
            }
        }
    }
}
