﻿using System;
using System.Collections.Generic;

namespace Employee_Management
{
    class Employee_operation
    {
        EmployeeInfo Emp = new EmployeeInfo();
        public EmployeeInfo[] infoarr = new EmployeeInfo[10];
        int emp_no = 10, emp_no1 = 0;

        public static void Array_s()
        {
            Employee employee = new Employee();
            bool Exit = true;

            do
            {
                Console.WriteLine("Press 1 to Add an Employee");
                Console.WriteLine("Press 2 to Delete an Employee");
                Console.WriteLine("Press 3 to Get details of an Employee");
                Console.WriteLine("Press 4 to Get details of all Employee");
                Console.WriteLine("Press any other character to exit from the Collections");
                int opt = Convert.ToInt16(Console.ReadLine());

                switch (opt)
                {
                    case 1:
                        employee.add_emp_arr();
                        break;
                    case 2:
                        employee.Del_emp_arr();
                        break;
                    case 3:
                        employee.Dis_emp_arr();
                        break;
                    case 4:
                        employee.dis_emp_arr();
                        break;
                    default:
                        Exit = false;
                        break;
                }

            }
            while (Exit);
        }

        //Arrya Operations
        public void add_emp_arr()
        {
            Console.WriteLine("Please enter the firstname");
            string firstname = Console.ReadLine();
            Console.WriteLine("Please enter the lastname");
            string lastname = Console.ReadLine();
            start:
            Console.WriteLine("Please enter the email Id");
            string email = Console.ReadLine();
            for (int i = 0; i < (infoarr.Length - emp_no); i++)
            {
                if (infoarr[i].Email == email)
                {
                    Console.WriteLine("Email already exists");
                    goto start;
                }
            }
            Console.WriteLine("Please enter the salary");
            double salary = Convert.ToDouble(Console.ReadLine());
            var employee = new EmployeeInfo()
            {
                Email = email,
                F_Name = firstname,
                L_Name = lastname,
                Salary = salary
            };

            infoarr[infoarr.Length - emp_no] = employee;
            emp_no1++;
            emp_no--;
            Console.WriteLine("employee added in array");
        }

        public void Dis_emp_arr()
        {
            bool flag = false;
            Console.WriteLine("Enter the email id of the required Employee");
            string search = Console.ReadLine();
            for (int i = 0; i < emp_no1; i++)
            {
                if (search == infoarr[i].Email)
                {
                    Console.WriteLine("Firstname= " + infoarr[i].F_Name + " Lastname= " + infoarr[i].L_Name + " Salary = " + infoarr[i].Salary + " Email = " + infoarr[i].Email);
                    flag = true;
                }
            }
            if (!flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
        }

        public void Del_emp_arr()
        {
            int i;
            bool flag = false;
            Console.WriteLine("Enter the email id of the employee to delete");
            string search = Console.ReadLine();
            for (i = 0; i < emp_no1; i++)
            {
                if (search == infoarr[i].Email)
                {
                    flag = true;
                    emp_no1--;
                }
                if (flag)
                {
                    infoarr[i] = infoarr[i + 1];
                }
            }
            infoarr[i] = null;
            if (!flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
            else
            {
                Console.WriteLine("Employee deleted successfully");
            }
        }

        public void dis_emp_arr()
        {
            for (int i = 0; i < emp_no1; i++)
            {
                Console.WriteLine("Firstname = " + infoarr[i].F_Name + " Lastname = " + infoarr[i].L_Name + " Salary = " + infoarr[i].Salary + " Email = " + infoarr[i].Email);

            }
        }

        //list Operations
        public void Add_emp_Data(List<EmployeeInfo> infos)
        {
            Console.Clear();
            Console.WriteLine("Enter First Name:");
            Emp.F_Name = Console.ReadLine();
            Console.WriteLine("Enter Last Name:");
            Emp.L_Name = Console.ReadLine();
            Console.WriteLine("Enter the salary");
            Emp.Salary = Convert.ToDouble(Console.ReadLine());
            start:
            Console.WriteLine("Enter Email ID");
            Emp.Email = Console.ReadLine();
            foreach(EmployeeInfo emp in infos)
            {
                if(emp.Email == Emp.Email)
                {
                    Console.WriteLine("Email already exists. Please enter new email");
                    goto start;
                }
            }
            infos.Add(Emp);
            Console.WriteLine("Employee Details Added");
        }

        public void Details_all_emp(List<EmployeeInfo> infos)
        {
            Console.Clear();

            foreach (EmployeeInfo emp in infos)
            {
                Console.WriteLine("First Name = "+emp.F_Name + " Last Name = " + emp.L_Name + " Salary = " + emp.Salary + " Email Address = " + emp.Email);
            }
            Console.ReadKey(false);
        }
        
        public void Details_emp(List<EmployeeInfo> infos)
        {
            Console.Clear();
            Console.WriteLine("Please enter the employee email address whose details are needed");
            foreach (var det in infos)
            {
                Console.WriteLine("First Name = "+det.F_Name+" last Name = "+det.L_Name+"Salary = "+det.Salary+" Email Address = "+det.Email);
            }

        }

        public void Delete_emp(List<EmployeeInfo> infos)
        {
            string email;
            Console.Clear();
            Console.WriteLine("Please enter the employee email address you want to delete");
            email = Console.ReadLine();
            foreach (var mail in infos)
            {
                infos.Remove(mail);
                break;
            }
            Console.WriteLine("Employee Deleted Successfully");
        }

        // Operations Dictionary
        public void Add_emp_dic()
        {            
            Console.WriteLine("Please enter the firstname");
            Emp.F_Name = Console.ReadLine();
            Console.WriteLine("Please enter the lastname");
            Emp.L_Name = Console.ReadLine();
            start:
            Console.WriteLine("Please enter the email Id");
            Emp.Email = Console.ReadLine();
            foreach(var add in Emp.KeyValues.Keys)
            {
                if(add == Emp.Email)
                {
                    Console.WriteLine("Email already exists. Please enter new email");
                    Emp.Email = Console.ReadLine();
                    goto start;
                }
            }
            Console.WriteLine("Please enter the salary");
            Emp.Salary = Convert.ToDouble(Console.ReadLine());
            var emp = new EmployeeInfo()
            {
                F_Name = Emp.F_Name,
                L_Name = Emp.L_Name,
                Salary = Emp.Salary,
                Email = Emp.Email
            };
            Emp.KeyValues.Add(emp.Email, emp);
            Console.WriteLine("Employee Details Added");
        }

        public void Del_emp_dic()
        {
            bool flag = true;
            Console.WriteLine("Enter the email id to delete");
            string search = Console.ReadLine();
            foreach(var emp in Emp.KeyValues.Values)
            {
                if(search == emp.Email)
                {
                    Emp.KeyValues.Remove(emp.Email);
                    Console.WriteLine("Employee Deleted Successfully");
                    flag = false;
                    break;
                }
                
            }
            if(flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
        }

        public void Detail_emp_dis()
        {
            bool flag = true;
            Console.WriteLine("Please enter the employee email address whose details are needed");
            string search = Console.ReadLine();
            foreach(var emp in Emp.KeyValues.Values)
            {
                if (search == emp.Email)
                {
                    Console.WriteLine("First Name = "+ emp.F_Name);
                    flag = false;
                    break;
                }
            }
            if (flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
        }

        public void Dis_emp_dic()
        {
            int count = 0;
            foreach(var emp in Emp.KeyValues.Values)
            {
                Console.WriteLine("Firstname= " + emp.F_Name + " Lastname= " + emp.L_Name + " Salary= " + emp.Salary + " Email= " + emp.Email);
                count++;
            }
            if(count==0)
            {
                Console.WriteLine("Employee does't exites in Dictionary");
            }
        }
    }
}

