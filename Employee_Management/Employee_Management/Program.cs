﻿using System;
using System.Collections.Generic;

namespace Employee_Management
{
    class Employee : Employee_operation
    {
        static void Main(string[] args)
        {
            Gen gen = new Gen();
            EmployeeInfo info = new EmployeeInfo();
            bool Exit = true;
            do
            {
                Console.WriteLine("Press 1 to implement Array");
                Console.WriteLine("Press 2 to implement List");
                Console.WriteLine("Press 3 to implement Dictionary");
                Console.WriteLine("Press any other charater to exit");
                int ope = Convert.ToInt16(Console.ReadLine());

                switch (ope)
                {
                    case 1:
                        Array_s();
                        break;
                    case 2:
                        gen.check<List<EmployeeInfo>>(info.infos);
                        break;
                    case 3:
                        gen.check<Dictionary<string, EmployeeInfo>>(info.KeyValues);
                        break;
                    default:
                        Exit = false;
                        break;
                }
                Console.ReadLine();
            } while (Exit);
        }
    }
}

